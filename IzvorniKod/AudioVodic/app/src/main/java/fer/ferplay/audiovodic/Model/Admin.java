package fer.ferplay.audiovodic.Model;

import java.io.Serializable;

/**
 * Created by Karlo on 13.1.2018..
 */

public class Admin implements Serializable {
    private String email;
    private String id;

    public Admin() {
    }

    public Admin(String email,String id) {
        this.email = email;
        this.id = id;
    }


    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Admin admin = (Admin) o;


        if (email != null ? !email.equals(admin.email) : admin.email != null) return false;

        return id != null ? id.equals(admin.id) : admin.id == null;

    }

    @Override
    public int hashCode() {
        int result = 13;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        return result;
    }
}
