package fer.ferplay.audiovodic.Data;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.List;

import fer.ferplay.audiovodic.Model.Promo;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.Constants;
import fer.ferplay.audiovodic.Util.DatabaseNodes.PromoNode;

/**
 * Created by Asus on 11/01/2018.
 */

public class PromoDeleteRecycleAdapter extends RecyclerView.Adapter<PromoDeleteRecycleAdapter.ViewHolder> {

    private Context context;
    private List<Promo> promoList;

    private FirebaseDatabase mDatabase;
    private FirebaseStorage storage;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    public PromoDeleteRecycleAdapter(Context context, List<Promo> promoList) {
        this.context = context;
        this.promoList = promoList;

        mDatabase = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();
    }

    @Override
    public PromoDeleteRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exhibit_row, parent, false);
        return new PromoDeleteRecycleAdapter.ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(PromoDeleteRecycleAdapter.ViewHolder holder, int position) {
        Promo promo = promoList.get(position);
        String imageUrl = null;

        holder.exhibitTitle.setText(promo.getTitle());
        holder.exhibitDesc.setText(promo.getDescription());

        imageUrl = promo.getImageLink();
        if (!imageUrl.equals(Constants.NONE)) {
            Picasso.with(context).load(imageUrl).into(holder.exhibitImage);
        }
    }

    @Override
    public int getItemCount() {
        return promoList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public ImageView exhibitImage;
        public TextView exhibitTitle;
        public TextView exhibitDesc;

        public ViewHolder(View view, Context ctx) {
            super(view);
            context = ctx;
            exhibitImage = (ImageView) view.findViewById(R.id.rowExhibitImageID);
            exhibitTitle = (TextView) view.findViewById(R.id.rowExhibitTitleID);
            exhibitDesc = (TextView) view.findViewById(R.id.rowExhibitDescriptionID);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Promo promo = promoList.get(getAdapterPosition());

            DatabaseReference databaseRef = mDatabase.getReference()
                    .child(PromoNode.NODE_NAME).child(promo.getId());

            StorageReference storageRef = storage.getReference()
                    .child("exhibit_image").child(promo.getId());

            builder = new AlertDialog.Builder(context);

            builder.setTitle("Title")
                    .setMessage("Are you sure you want to delete the promo?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            databaseRef.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    storageRef.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(context, "Promo deleted!", Toast.LENGTH_SHORT);
                                        }
                                    });
                                }
                            });
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }
    }
}
