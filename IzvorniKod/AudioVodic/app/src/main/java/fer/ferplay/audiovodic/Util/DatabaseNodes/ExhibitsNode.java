package fer.ferplay.audiovodic.Util.DatabaseNodes;

/**
 * Created by Mob on 25.10.2017..
 */

public class ExhibitsNode {
    public static final String NODE_NAME = "exhibits";
    public static final String CHILD_AUDIOLINK = "audioLink";
    public static final String CHILD_DESC = "description";
    public static final String CHILD_ID = "id";
    public static final String CHILD_IMAGELINK = "imageLink";
    public static final String CHILD_TITLE ="title";
}
