package fer.ferplay.audiovodic.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fer.ferplay.audiovodic.Data.DemoteRecycleAdapter;
import fer.ferplay.audiovodic.Model.Admin;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.AdminsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class DemoteActivity extends AppCompatActivity {
    private FirebaseDatabase mDatabase;
    private DatabaseReference mAdminRef;
    private RecyclerView recyclerView;
    private DemoteRecycleAdapter adminRecycleAdapter;
    private List<User> adminList;
    private boolean flag;
    private Button addAdmins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_demote);

        addAdmins = (Button) findViewById(R.id.add_admin);

        mDatabase = FirebaseDatabase.getInstance();
        mAdminRef = mDatabase.getReference().child(AdminsNode.NODE_NAME);

        adminList = new ArrayList<>();
        adminRecycleAdapter = new DemoteRecycleAdapter(this, adminList);
        recyclerView = (RecyclerView) findViewById(R.id.adminListID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(adminRecycleAdapter);

        Toast.makeText(DemoteActivity.this,
                "Click on admin to demote him",
                Toast.LENGTH_SHORT).show();
        mAdminRef.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Admin admin = dataSnapshot.getValue(Admin.class);
                flag=false;
                for(User user1:adminList){
                    if(admin.getID().equals(user1.getUserID())) flag=true;
                }
                if(!flag){//po ID!

                    DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(admin.getID());
                    userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User user=dataSnapshot.getValue(User.class);
                            if(user.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                                adminList.add(user);
                                adminRecycleAdapter.notifyDataSetChanged();
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }

                    });
                }
            }

            //pogledati
            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                /*User user = dataSnapshot.getValue(User.class);
                int pos = adminList.indexOf(user);
                adminList.add(pos,user);
                adminRecycleAdapter.notifyItemChanged(pos);*/

                for(User u:adminList){
                    if(!u.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)){
                        adminList.remove(u);
                        adminRecycleAdapter.notifyDataSetChanged();
                    }
                }
            }

            //pogledati, problem sa snaphotom jer su razlicite klase
            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
               /* User user = dataSnapshot.getValue(User.class);
                if(adminList.contains(user)){
                    int pos = adminList.indexOf(user);
                    adminList.remove(pos);
                    adminRecycleAdapter.notifyItemChanged(pos);
                }*/
               Admin a= dataSnapshot.getValue(Admin.class);
               if(a!=null) {
                   Toast.makeText(DemoteActivity.this, "admin removed", Toast.LENGTH_SHORT).show();
                   int i = -1;
                   for (User u : adminList)
                       if (u.getUserID().equals(a.getID()))
                            i=adminList.indexOf(u);
                   if(i!=-1) {
                       adminList.remove(i);
                       adminRecycleAdapter.notifyDataSetChanged();
                   }
               }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        addAdmins.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (adminList.size() >= 5) {
                    Toast.makeText(DemoteActivity.this,
                            "There is already maximum number of admins!",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                startActivity(new Intent(v.getContext(), PromoteActivity.class));
            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void goToLoginScreen(){
        Intent intent = new Intent(DemoteActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
