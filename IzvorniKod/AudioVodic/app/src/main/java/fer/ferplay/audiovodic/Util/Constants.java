package fer.ferplay.audiovodic.Util;

/**
 * Created by Mob on 24.10.2017..
 */

public class Constants {
    public static final String EXHIBIT_INTENT_KEY = "exhibit";
    public static final String PROMO_INTENT_KEY = "promo";
    public static final String NONE = "none";
    public static final String BARCODE_INTENT_KEY = "barcode";
}
