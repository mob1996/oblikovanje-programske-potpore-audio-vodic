package fer.ferplay.audiovodic.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fer.ferplay.audiovodic.Data.ExhibitEditRecycleAdapter;
import fer.ferplay.audiovodic.Data.PromoDeleteRecycleAdapter;
import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.Promo;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.ExhibitsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.PromoNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class DeletePromoActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PromoDeleteRecycleAdapter recycleAdapter;
    private List<Promo> promoList;

    private FirebaseDatabase database;
    private DatabaseReference promosRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_promo);

        Toast.makeText(DeletePromoActivity.this,
                "Press on the promo you want to delete", Toast.LENGTH_SHORT).show();

        database = FirebaseDatabase.getInstance();
        promosRef = database.getReference().child(PromoNode.NODE_NAME);

        promoList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.promoListID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        recycleAdapter = new PromoDeleteRecycleAdapter(DeletePromoActivity.this, promoList);
        recyclerView.setAdapter(recycleAdapter);

        promosRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Promo promo = dataSnapshot.getValue(Promo.class);
                if (!promoList.contains(promo)) {
                    promoList.add(promo);
                    recycleAdapter.notifyItemInserted(promoList.size() - 1);
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Promo promo = dataSnapshot.getValue(Promo.class);
                int pos = promoList.indexOf(promo);
                promoList.add(pos, promo);
                recycleAdapter.notifyItemChanged(pos);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Promo promo = dataSnapshot.getValue(Promo.class);
                if (promoList.contains(promo)) {
                    int pos = promoList.indexOf(promo);
                    promoList.remove(pos);
                    recycleAdapter.notifyItemRemoved(pos);
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }
    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = database.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = database.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = database.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = database.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = database.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = database.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void goToLoginScreen(){
        Intent intent = new Intent(DeletePromoActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
