package fer.ferplay.audiovodic.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.Map;

import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.Constants;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.StatsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class ExhibitActivity extends AppCompatActivity {

    private TextView exhibitTitle;
    private ImageView exhibitImage;
    private TextView exhibitDescription;
    private Exhibit exhibit;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference generalStatsRef;
    private DatabaseReference exibitStatsRef;

    private long beginTimeCount;
    private long endTimeCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exhibit);

        exhibitTitle = (TextView) findViewById(R.id.exhibitTitleID);
        exhibitImage = (ImageView) findViewById(R.id.exhibitImageID);
        exhibitDescription = (TextView) findViewById(R.id.exhibitDescriptionID);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();

        exhibit = (Exhibit) getIntent().getSerializableExtra(Constants.EXHIBIT_INTENT_KEY);

        exhibitTitle.setText(exhibit.getTitle());
        exhibitDescription.setText(exhibit.getDescription());
        if(!exhibit.getImageLink().equals(Constants.NONE)){
            Picasso.with(getApplicationContext()).load(exhibit.getImageLink()).into(exhibitImage);
        }

        if(exhibitTitle.toString().contains("promo")) return;

        generalStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_GENERAL);
        exibitStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME)
                .child(StatsNode.CHILD_EXHIBITS_STATS).child(exhibit.getId());

        beginTimeCount = System.currentTimeMillis();


    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
        if(exhibitTitle.toString().contains("promo")) return;

        generalStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long totalVisits = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long totalVisitsLong = (Long) data.get(StatsNode.CHILD_TOTAL_VISITS);
                    totalVisits = totalVisitsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                totalVisits++;;
                data.put(StatsNode.CHILD_TOTAL_VISITS, totalVisits);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

        exibitStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long visits = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null) {
                    data = (Map) currentData.getValue();
                    Long visitsLong = (Long) data.get(StatsNode.CHILD_VISITS);
                    visits = visitsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                visits++;
                data.put(StatsNode.CHILD_VISITS, visits);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        if(exhibitTitle.toString().contains("promo")) return;

        endTimeCount = System.currentTimeMillis();
        Log.d("ExhibitActivity", "OnPause");
        generalStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long totalVisitTime = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long totalVisitTimeLong = (Long) data.get(StatsNode.CHILD_TOTAL_VISIT_TIME);
                    totalVisitTime = totalVisitTimeLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                totalVisitTime += (endTimeCount-beginTimeCount);
                data.put(StatsNode.CHILD_TOTAL_VISIT_TIME, totalVisitTime);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
        exibitStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long visitTime = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long visitTimeLong = (Long) data.get(StatsNode.CHILD_VISIT_TIME);
                    visitTime = visitTimeLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                visitTime += (endTimeCount-beginTimeCount);
                data.put(StatsNode.CHILD_VISIT_TIME, visitTime);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_signout:
                actionSignOutExhibitActivity();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.action_scan).setVisible(false);
        menu.findItem(R.id.action_go_to_hq).setVisible(false);
        menu.findItem(R.id.action_go_to_front).setVisible(false);
        return super.onCreateOptionsMenu(menu);
    }

    public void goToLoginScreen(){
        Intent intent = new Intent(ExhibitActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


    private void actionSignOutExhibitActivity(){
        if(mAuth.getCurrentUser()!=null){
            if(!mAuth.getCurrentUser().isAnonymous()){
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).
                        child(mAuth.getCurrentUser().getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        DatabaseReference onlineRef;
                        if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                            onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                        } else {
                            onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                        }
                        onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                mAuth.signOut();
                                Toast.makeText(ExhibitActivity.this, "Sign Out!", Toast.LENGTH_LONG).show();
                                goToLoginScreen();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                mAuth.signOut();
                goToLoginScreen();
            }
        }
    }


}
