package fer.ferplay.audiovodic.Data;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.AdminsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.PromoNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

/**
 * Created by Karlo on 13.1.2018..
 */

public class DemoteRecycleAdapter extends RecyclerView.Adapter<DemoteRecycleAdapter.ViewHolder> {
    private FirebaseDatabase mDatabase;
    private Context context;
    private List<User> userList;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    public DemoteRecycleAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
        mDatabase = FirebaseDatabase.getInstance();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.online_user, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);

        holder.userEmail.setText(user.getEmail());
        holder.userAuthLevel.setText("Auth level: " + user.getAuthLevel());
        holder.userFirstName.setText("First Name: " + user.getFirst_name());
        holder.userLastName.setText("Last Name: " + user.getLast_name());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView userEmail;
        public TextView userAuthLevel;
        public TextView userFirstName;
        public TextView userLastName;

        public ViewHolder(View view, Context ctx) {
            super(view);
            userEmail = (TextView) view.findViewById(R.id.userEmail);
            userAuthLevel = (TextView) view.findViewById(R.id.userAuthLevel);
            userFirstName=view.findViewById(R.id.userFirstName);
            userLastName=view.findViewById(R.id.userLastName);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            User user = userList.get(getAdapterPosition());
            DatabaseReference databaseRef = mDatabase.getReference()
                    .child(UsersNode.NODE_NAME).child(user.getUserID()).child(UsersNode.CHILD_AUTH);
            DatabaseReference databaseRefAdmin = mDatabase.getReference()
                    .child(AdminsNode.NODE_NAME).child(user.getUserID());

            builder = new AlertDialog.Builder(context);

            builder.setTitle("Warning")
                    .setMessage("Are you sure you want to demote admin?")
                    .setIcon(android.R.drawable.ic_dialog_alert)
                    .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {

                            databaseRef.setValue(AuthLevel.AUTH_USER).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                }
                            });
                            databaseRefAdmin.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                }
                            });
                        }
                    })
                    .setNegativeButton(android.R.string.no, null).show();
        }

    }
}