package fer.ferplay.audiovodic.Activities;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.Constants;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.StatsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class PlayAudioActivity extends AppCompatActivity {

    private TextView titleField;
    private SeekBar seekBar;
    private TextView playingTime;
    private TextView remainingTime;
    private Button playPauseBtn;
    private ImageView exhibitImage;

    private Exhibit exhibit;
    private MediaPlayer player;
    private Thread seekBarThread;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private FirebaseUser mUser;
    private DatabaseReference generalStatsRef;
    private DatabaseReference exibitStatsRef;

    private long beginTimeCount;
    private long endTimeCount;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_play_audio);

        titleField = (TextView) findViewById(R.id.playAudioTitleID);
        seekBar = (SeekBar) findViewById(R.id.playAudioSeekBarID);
        playingTime = (TextView) findViewById(R.id.playAudioPlayingTimeID);
        remainingTime = (TextView) findViewById(R.id.playAudioRemainingTimeID);
        playPauseBtn = (Button) findViewById(R.id.playAudioPlayPauseBtn);
        exhibitImage = (ImageView) findViewById(R.id.playAudioExhibitImageID);

        exhibit = (Exhibit) getIntent().getSerializableExtra(Constants.EXHIBIT_INTENT_KEY);
        player = new MediaPlayer();

        mDatabase = FirebaseDatabase.getInstance();
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        generalStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_GENERAL);
        exibitStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME)
                .child(StatsNode.CHILD_EXHIBITS_STATS).child(exhibit.getId());

        beginTimeCount = System.currentTimeMillis();


        setUpUI();

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser){
                    player.seekTo(progress);
                }
                updateTime();
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        playPauseBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(player!=null){
                    if(player.isPlaying()){
                        player.pause();
                        playPauseBtn.setBackgroundResource(R.drawable.ic_media_play);
                    } else {
                        player.start();
                        playPauseBtn.setBackgroundResource(R.drawable.ic_media_pause);
                        updateSeekBar();
                    }
                }
            }
        });

        playAudio();


    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }

        generalStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long totalVisits = 0;
                long totalReproductions = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long totalVisitsLong = (Long) data.get(StatsNode.CHILD_TOTAL_VISITS);
                    Long totalReproductionsLong = (Long) data.get(StatsNode.CHILD_TOTAL_REPRODUCTIONS);
                    totalVisits = totalVisitsLong.longValue();
                    totalReproductions = totalReproductionsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                totalVisits++;;
                totalReproductions++;
                data.put(StatsNode.CHILD_TOTAL_VISITS, totalVisits);
                data.put(StatsNode.CHILD_TOTAL_REPRODUCTIONS, totalReproductions);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

        exibitStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long visits = 0;
                long reproductions = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null) {
                    data = (Map) currentData.getValue();
                    Long visitsLong = (Long) data.get(StatsNode.CHILD_VISITS);
                    Long reproductionsLong = (Long) data.get(StatsNode.CHILD_REPRODUCTIONS);
                    visits = visitsLong.longValue();
                    reproductions = reproductionsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                visits++;
                reproductions++;
                data.put(StatsNode.CHILD_VISITS, visits);
                data.put(StatsNode.CHILD_REPRODUCTIONS, reproductions);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean commited, DataSnapshot dataSnapshot) {
                if(commited){
                    if(!mUser.isAnonymous()){
                        exibitStatsRef.child(StatsNode.CHILD_UNIQUE).child(mUser.getUid())
                                .runTransaction(new Transaction.Handler() {
                                    @Override
                                    public Transaction.Result doTransaction(MutableData currentData) {
                                        if(currentData!=null){
                                            currentData.setValue(mUser.getEmail());
                                        } else {
                                            return Transaction.success(currentData);
                                        }
                                        return Transaction.success(currentData);
                                    }

                                    @Override
                                    public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

                                    }
                                });

                    }
                }
            }
        });


    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        endTimeCount = System.currentTimeMillis();
        generalStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long totalVisitTime = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long totalVisitTimeLong = (Long) data.get(StatsNode.CHILD_TOTAL_VISIT_TIME);
                    totalVisitTime = totalVisitTimeLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                totalVisitTime += (endTimeCount-beginTimeCount);
                data.put(StatsNode.CHILD_TOTAL_VISIT_TIME, totalVisitTime);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

        exibitStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long visitTime = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long visitTimeLong = (Long) data.get(StatsNode.CHILD_VISIT_TIME);
                    visitTime = visitTimeLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                visitTime += (endTimeCount-beginTimeCount);
                data.put(StatsNode.CHILD_VISIT_TIME, visitTime);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });
    }


    @Override
    protected void onDestroy() {
        Log.d("TAG", "OnPause");
        if(seekBarThread != null){
            if(seekBarThread.isAlive()){
                seekBarThread.interrupt();
                seekBarThread = null;
            }
        }
        if(player != null){
            player.stop();
            player.release();
            Log.d("Release", "PlayerRelease");
            player = null;
        }
        try {
            trimCache(this);
        } catch (Exception e){
            e.printStackTrace();
        }
        super.onDestroy();
        Log.d("TAG", "OnDestroy");

    }

    private void setUpUI(){
        titleField.setText(exhibit.getTitle());
        Picasso.with(getApplicationContext()).load(exhibit.getImageLink()).into(exhibitImage);
    }


    private void updateSeekBar (){
        seekBarThread = new Thread(){
            @Override
            public void run(){
                try{
                    while (player != null && player.isPlaying()){
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                int newPosition = player.getCurrentPosition();
                                int duration = player.getDuration();
                                seekBar.setMax(duration);
                                seekBar.setProgress(newPosition);

                                updateTime();
                            }
                        });
                    }
                } catch (InterruptedException e){
                    e.printStackTrace();
                }
            }
        };
        seekBarThread.start();
    }

    private void playAudio(){
        try{
            player.setDataSource(PlayAudioActivity.this, Uri.parse(exhibit.getAudioLink()));
            Log.d("Player", Uri.parse(exhibit.getAudioLink()).toString());
            player.prepare();
            player.start();
            player.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                @Override
                public void onCompletion(MediaPlayer mediaPlayer) {
                    mediaPlayer.seekTo(0);
                    mediaPlayer.pause();
                    playPauseBtn.setBackgroundResource(R.drawable.ic_media_play);
                    seekBar.setProgress(0);


                }
            });
            updateSeekBar();
        } catch (Exception e){
            Toast.makeText(getApplicationContext(), "Not playable audio URI: "+exhibit.getAudioLink(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
            trimCache(getApplicationContext());
            finish();
        }
    }



    private void trimCache(Context context){
        try{
            File dir = context.getCacheDir();
            if(dir!=null && dir.isDirectory()){
                deleteDir(dir);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private boolean deleteDir(File dir){
        if(dir!=null && dir.isDirectory()){
            String[] children = dir.list();
            for(String child:children){
                boolean success = deleteDir(new File(dir, child));
                if(!success){
                    return false;
                }
            }
        }
        return dir.delete();
    }

    private void updateTime(){
        SimpleDateFormat dateFormat = new SimpleDateFormat("mm:ss");
        int currentPositon = player.getCurrentPosition();
        int duration = player.getDuration();

        playingTime.setText(dateFormat.format(new Date(currentPositon)));
        remainingTime.setText(dateFormat.format(new Date(duration-currentPositon)));
    }



    public void goToLoginScreen(){
        Intent intent = new Intent(PlayAudioActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }
}
