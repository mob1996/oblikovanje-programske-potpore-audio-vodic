package fer.ferplay.audiovodic.Data;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fer.ferplay.audiovodic.Activities.EditDeleteExhibitActivity;
import fer.ferplay.audiovodic.Activities.EditDeleteExhibitsActivity;
import fer.ferplay.audiovodic.Activities.ExhibitActivity;
import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.Constants;

/**
 * Created by Asus on 07/01/2018.
 */

public class ExhibitEditRecycleAdapter extends RecyclerView.Adapter<ExhibitEditRecycleAdapter.ViewHolder>{

    private Context context;
    private List<Exhibit> exhibitList;

    public ExhibitEditRecycleAdapter(Context context, List<Exhibit> exhibitList) {
        this.context = context;
        this.exhibitList = exhibitList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exhibit_row, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Exhibit exhibit = exhibitList.get(position);
        String imageUrl = null;

        holder.exhibitTitle.setText(exhibit.getTitle());
        holder.exhibitDesc.setText(exhibit.getDescription());

        imageUrl = exhibit.getImageLink();
        if(!imageUrl.equals(Constants.NONE)){
            Picasso.with(context).load(imageUrl).into(holder.exhibitImage);
        }
    }

    @Override
    public int getItemCount() {
        return exhibitList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public ImageView exhibitImage;
        public TextView exhibitTitle;
        public TextView exhibitDesc;

        public ViewHolder(View view, Context ctx) {
            super(view);
            context = ctx;
            exhibitImage = (ImageView) view.findViewById(R.id.rowExhibitImageID);
            exhibitTitle = (TextView) view.findViewById(R.id.rowExhibitTitleID);
            exhibitDesc = (TextView) view.findViewById(R.id.rowExhibitDescriptionID);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Exhibit exhibit = exhibitList.get(getAdapterPosition());
            Intent intent = new Intent(context, EditDeleteExhibitActivity.class);
            intent.putExtra(Constants.EXHIBIT_INTENT_KEY, exhibit);
            context.startActivity(intent);
        }
    }
}
