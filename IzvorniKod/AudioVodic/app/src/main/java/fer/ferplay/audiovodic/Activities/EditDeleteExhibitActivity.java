package fer.ferplay.audiovodic.Activities;

import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.ExhibitStats;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.Constants;
import fer.ferplay.audiovodic.Util.DatabaseNodes.ExhibitsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.StatsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class EditDeleteExhibitActivity extends AppCompatActivity {

    private EditText titleField;
    private EditText descriptionField;
    private EditText imageURIField;
    private Button imageURIBtn;
    private EditText audioURIField;
    private Button audioURIBtn;
    private Button editExhibitBtn;
    private Button deleteExhibitBtn;

    private FirebaseDatabase mDatabase;

    private FirebaseStorage storage;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    Exhibit exhibit;
    ExhibitStats exhibitStats;

    private static final int IMAGE_SELECT_CODE = 0;
    private static final int AUDIO_SELECT_CODE = 1;
    private static final int MAX_AUDIO_DURATION = 180000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_delete_exhibit);

        titleField = (EditText) findViewById(R.id.exhibitTitleEdit);
        descriptionField = (EditText) findViewById(R.id.exhibitDescriptionEdit);
        imageURIField = (EditText) findViewById(R.id.imageUriEdit);
        imageURIBtn = (Button) findViewById(R.id.chooseImageEdit);
        audioURIField = (EditText) findViewById(R.id.audioUriEdit);
        audioURIBtn = (Button) findViewById(R.id.chooseAudioEdit);
        editExhibitBtn = (Button) findViewById(R.id.editExhibit);
        deleteExhibitBtn = (Button) findViewById(R.id.deleteExhibit);

        mDatabase = FirebaseDatabase.getInstance();
        storage = FirebaseStorage.getInstance();

        exhibit = (Exhibit) getIntent().getSerializableExtra(Constants.EXHIBIT_INTENT_KEY);

        titleField.setText(exhibit.getTitle());
        descriptionField.setText(exhibit.getDescription());

        DatabaseReference databaseRefStats = mDatabase.getReference()
                .child(StatsNode.NODE_NAME).child(StatsNode.CHILD_EXHIBITS_STATS).child(exhibit.getId());

        databaseRefStats.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                exhibitStats = dataSnapshot.getValue(ExhibitStats.class);

                if(exhibitStats == null) {
                    exhibitStats = new ExhibitStats(
                            0L,
                            0L,
                            0L,
                            exhibit.getTitle(),
                            "none");
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        DatabaseReference databaseRefExhibits = mDatabase.getReference()
                .child(ExhibitsNode.NODE_NAME).child(exhibit.getId());

        StorageReference storageRefImage = storage.getReference()
                .child("exhibit_image").child(exhibit.getId());
        StorageReference storageRefAudio = storage.getReference()
                .child("exhibit_audio").child(exhibit.getId());

        imageURIBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(
                            Intent.createChooser(intent, "Select an Image to Upload"),
                            IMAGE_SELECT_CODE);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(
                            EditDeleteExhibitActivity.this,
                            "There is no file manager installed!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });

        audioURIBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("audio/*");
                intent.addCategory(Intent.CATEGORY_OPENABLE);

                try {
                    startActivityForResult(
                            Intent.createChooser(intent, "Select an Audio to Upload"),
                            AUDIO_SELECT_CODE);
                } catch (ActivityNotFoundException ex) {
                    Toast.makeText(
                            EditDeleteExhibitActivity.this,
                            "There is no file manager installed!",
                            Toast.LENGTH_SHORT).show();
                }
            }
        });


        deleteExhibitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                builder = new AlertDialog.Builder(EditDeleteExhibitActivity.this);
                builder.setTitle("Title")
                        .setMessage("Are you sure you want to delete the exhibit?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                databaseRefExhibits.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        databaseRefStats.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                storageRefImage.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        storageRefAudio.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                Toast.makeText(EditDeleteExhibitActivity.this,
                                                                        "Exhibit removed.", Toast.LENGTH_SHORT).show();
                                                                finish();
                                                            }
                                                        }).addOnFailureListener(new OnFailureListener() {
                                                            @Override
                                                            public void onFailure(@NonNull Exception e) {
                                                                Toast.makeText(EditDeleteExhibitActivity.this,
                                                                        "Exhibit removed.", Toast.LENGTH_SHORT).show();
                                                                finish();
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }})
                        .setNegativeButton(android.R.string.no, null).show();
            }
        });

        editExhibitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String title = titleField.getText().toString();
                final String description = descriptionField.getText().toString();
                final String imageURI = imageURIField.getText().toString();
                final String audioURI = audioURIField.getText().toString();

                if (title.isEmpty() || description.isEmpty()) {
                    Toast.makeText(EditDeleteExhibitActivity.this,
                            "The title and description fields must not be empty!",
                            Toast.LENGTH_SHORT).show();
                    return;
                }

                exhibit.setTitle(title);
                exhibit.setDescription(description);
                exhibitStats.setTitle(title);

                DatabaseReference databaseRefExhibit = mDatabase.getReference()
                        .child(ExhibitsNode.NODE_NAME).child(exhibit.getId());

                DatabaseReference databaseRefStats = mDatabase.getReference()
                        .child(StatsNode.NODE_NAME).child(StatsNode.CHILD_EXHIBITS_STATS)
                        .child(exhibit.getId());

                if(!imageURI.isEmpty()) {
                    Uri image = Uri.parse(imageURI);

                    UploadTask uploadTaskImage = storageRefImage.putFile(image);

                    builder = new AlertDialog.Builder(EditDeleteExhibitActivity.this);
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    builder.setView(inflater.inflate(R.layout.progress_dialog_image, null));
                    builder.setCancelable(false);
                    alertDialog = builder.create();

                    alertDialog.show();

                    uploadTaskImage.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(EditDeleteExhibitActivity.this,
                                    "Image upload failed!", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(EditDeleteExhibitActivity.this,
                                    "Image upload succeeded!", Toast.LENGTH_SHORT).show();

                            alertDialog.dismiss();

                            storageRefImage.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

                                @Override
                                public void onSuccess(Uri uri) {
                                    exhibit.setImageLink(uri.toString());

                                    exhibitStats.setImageLink(uri.toString());

                                    if (!audioURI.isEmpty()) {
                                        Uri audio = Uri.parse(audioURI);

                                        UploadTask uploadTaskAudio = storageRefAudio.putFile(audio);

                                        builder = new AlertDialog.Builder(EditDeleteExhibitActivity.this);
                                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        builder.setView(inflater.inflate(R.layout.progress_dialog_audio, null));
                                        builder.setCancelable(false);
                                        alertDialog = builder.create();

                                        alertDialog.show();

                                        uploadTaskAudio.addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Toast.makeText(EditDeleteExhibitActivity.this,
                                                        "Audio upload failed!", Toast.LENGTH_SHORT).show();
                                            }
                                        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                                            @Override
                                            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                                                Toast.makeText(EditDeleteExhibitActivity.this,
                                                        "Audio upload succeeded!", Toast.LENGTH_SHORT).show();

                                                alertDialog.dismiss();

                                                storageRefAudio.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

                                                    @Override
                                                    public void onSuccess(Uri uri) {
                                                        exhibit.setAudioLink(uri.toString());

                                                        databaseRefExhibit.setValue(exhibit).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                            @Override
                                                            public void onComplete(@NonNull Task<Void> task) {
                                                                Toast.makeText(EditDeleteExhibitActivity.this,
                                                                        "Exhibit added!", Toast.LENGTH_SHORT).show();
                                                                databaseRefStats.setValue(exhibitStats).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                                    @Override
                                                                    public void onComplete(@NonNull Task<Void> task) {
                                                                        finish();
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                }).addOnFailureListener(new OnFailureListener() {
                                                    @Override
                                                    public void onFailure(@NonNull Exception exception) {
                                                    }
                                                });
                                            }
                                        });
                                    } else {

                                        databaseRefExhibit.setValue(exhibit).addOnCompleteListener(new OnCompleteListener<Void>() {
                                            @Override
                                            public void onComplete(@NonNull Task<Void> task) {
                                                Toast.makeText(EditDeleteExhibitActivity.this,
                                                        "Exhibit edited!", Toast.LENGTH_SHORT).show();
                                                databaseRefStats.setValue(exhibitStats).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task) {
                                                        finish();
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                }
                            });

                        }
                    });
                } else if(!audioURI.isEmpty()) {
                    Uri audio = Uri.parse(audioURI);

                    UploadTask uploadTaskAudio = storageRefAudio.putFile(audio);

                    builder = new AlertDialog.Builder(EditDeleteExhibitActivity.this);
                    LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    builder.setView(inflater.inflate(R.layout.progress_dialog_audio, null));
                    builder.setCancelable(false);
                    alertDialog = builder.create();

                    alertDialog.show();

                    uploadTaskAudio.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Toast.makeText(EditDeleteExhibitActivity.this,
                                    "Audio upload failed!", Toast.LENGTH_SHORT).show();
                        }
                    }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            Toast.makeText(EditDeleteExhibitActivity.this,
                                    "Audio upload succeeded!", Toast.LENGTH_SHORT).show();

                            alertDialog.dismiss();

                            storageRefAudio.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {

                                @Override
                                public void onSuccess(Uri uri) {
                                    exhibit.setAudioLink(uri.toString());

                                    databaseRefExhibit.setValue(exhibit).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            Toast.makeText(EditDeleteExhibitActivity.this,
                                                    "Exhibit edited!", Toast.LENGTH_SHORT).show();
                                            databaseRefStats.setValue(exhibitStats).addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    finish();
                                                }
                                            });
                                        }
                                    });
                                }
                            }).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception exception) {
                                }
                            });
                        }
                    });
                } else {
                    databaseRefExhibit.setValue(exhibit).addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                            Toast.makeText(EditDeleteExhibitActivity.this,
                                    "Exhibit edited!", Toast.LENGTH_SHORT).show();
                            databaseRefStats.setValue(exhibitStats).addOnCompleteListener(new OnCompleteListener<Void>() {
                                @Override
                                public void onComplete(@NonNull Task<Void> task) {
                                    finish();
                                }
                            });
                        }
                    });
                }
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == IMAGE_SELECT_CODE) {
                imageURIField.setText(data.getData().toString());
            } else if (requestCode == AUDIO_SELECT_CODE) {

                Uri audio = data.getData();

                MediaMetadataRetriever mmr = new MediaMetadataRetriever();
                mmr.setDataSource(EditDeleteExhibitActivity.this, audio);
                String durationStr = mmr.extractMetadata(
                        MediaMetadataRetriever.METADATA_KEY_DURATION);
                int duration = Integer.parseInt(durationStr);

                if(duration > MAX_AUDIO_DURATION) {
                    Toast.makeText(
                            EditDeleteExhibitActivity.this,
                            "Audio duration too long. Needs to be shorter than 3 minutes!",
                            Toast.LENGTH_LONG).show();
                } else {
                    audioURIField.setText(data.getData().toString());
                }
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void goToLoginScreen(){
        Intent intent = new Intent(EditDeleteExhibitActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
