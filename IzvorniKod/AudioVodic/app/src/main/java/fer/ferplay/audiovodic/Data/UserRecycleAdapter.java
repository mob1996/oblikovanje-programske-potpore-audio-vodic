package fer.ferplay.audiovodic.Data;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.Constants;

/**
 * Created by matej on 22/12/2017.
 */

public class UserRecycleAdapter extends RecyclerView.Adapter<UserRecycleAdapter.ViewHolder> {
    private Context context;
    private List<User> userList;

    public UserRecycleAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
    }

    @Override
    public UserRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.online_user, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);

        holder.userEmail.setText(user.getEmail());
        holder.userAuthLevel.setText("Auth level: " + user.getAuthLevel());
        holder.userFirstName.setText("First Name: " + user.getFirst_name());
        holder.userLastName.setText("Last Name: " + user.getLast_name());
    }


    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public TextView userEmail;
        public TextView userAuthLevel;
        public TextView userFirstName;
        public TextView userLastName;

        public ViewHolder(View view, Context ctx) {
            super(view);
            userEmail = (TextView) view.findViewById(R.id.userEmail);
            userAuthLevel = (TextView) view.findViewById(R.id.userAuthLevel);
            userFirstName=view.findViewById(R.id.userFirstName);
            userLastName=view.findViewById(R.id.userLastName);
        }

    }
}
