package fer.ferplay.audiovodic.Activities;

import android.content.Intent;
import android.os.Bundle;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;


public class HQActivity extends AppCompatActivity {
    private FirebaseUser mUser;
    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private Button edit_del, addExhibit, addPromo, delPromo, stats, online, manage_admins;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hq);
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        edit_del=findViewById(R.id.edit_delete);
        addExhibit=findViewById(R.id.add);
        addPromo = findViewById(R.id.add_promo);
        delPromo = findViewById(R.id.delete_promo);
        stats=findViewById(R.id.stats);
        online=findViewById(R.id.online);
        manage_admins=findViewById(R.id.manage_admins);
        manage_admins.setVisibility(View.INVISIBLE);
        manage_admins.setOnClickListener(v->{
                    startActivity(new Intent(v.getContext(), DemoteActivity.class));
        });

        online.setOnClickListener(v->{
            startActivity(new Intent(v.getContext(), OnlineActivity.class));
        });
        stats.setOnClickListener(v->{
            startActivity(new Intent(v.getContext(), StatsActivity.class));
        });

        addExhibit.setOnClickListener(v->{
            startActivity(new Intent(v.getContext(), CreateExhibitActivity.class));
        });

        edit_del.setOnClickListener(v->{
            startActivity(new Intent(v.getContext(), EditDeleteExhibitsActivity.class));
        });

        addPromo.setOnClickListener(v-> {
            startActivity(new Intent(v.getContext(), CreatePromoActivity.class));
        });

        delPromo.setOnClickListener(v-> {
            startActivity(new Intent(v.getContext(), DeletePromoActivity.class));
        });

        mDatabase.getReference().child("users").child(mAuth.getCurrentUser().getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        if(user.getAuthLevel().equals(AuthLevel.AUTH_OWNER)){
                            findViewById(R.id.manage_admins).setVisibility(View.VISIBLE);
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void goToLoginScreen(){
        Intent intent = new Intent(HQActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
