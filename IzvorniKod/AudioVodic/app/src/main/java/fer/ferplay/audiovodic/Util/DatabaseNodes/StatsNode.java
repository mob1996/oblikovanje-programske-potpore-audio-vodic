package fer.ferplay.audiovodic.Util.DatabaseNodes;

/**
 * Created by Mob on 29.10.2017..
 */

public class StatsNode {
    public static String NODE_NAME = "stats";
    public static String CHILD_GENERAL = "general_stats";
    public static String CHILD_EXHIBITS_STATS = "exhibit_stats";
    public static String CHILD_FRONT_PAGE = "front_page_stats";
    public static String CHILD_UNIQUE = "unique_reproductions";
    public static String CHILD_TOTAL_VISITS = "totalVisits";
    public static String CHILD_TOTAL_REPRODUCTIONS = "totalReproductions";
    public static String CHILD_TOTAL_VISIT_TIME = "totalVisitTime";
    public static String CHILD_VISITS = "visits";
    public static String CHILD_REPRODUCTIONS = "reproductions";
    public static String CHILD_VISIT_TIME = "visitTime";
}
