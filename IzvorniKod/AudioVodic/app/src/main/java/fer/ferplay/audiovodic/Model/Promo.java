package fer.ferplay.audiovodic.Model;

import java.io.Serializable;

/**
 * Created by Karlo on 27.12.2017..
 */

public class Promo implements Serializable {

    private String id;
    private String title;
    private String description;
    private String imageLink;

    public Promo(){

    }

    public Promo(String id, String title, String description, String imageLink){
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageLink = imageLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Promo promo = (Promo) o;

        return id.equals(promo.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}

