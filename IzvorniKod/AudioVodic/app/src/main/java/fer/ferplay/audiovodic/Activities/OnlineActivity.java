package fer.ferplay.audiovodic.Activities;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fer.ferplay.audiovodic.Data.ExhibitRecycleAdapter;
import fer.ferplay.audiovodic.Data.UserRecycleAdapter;
import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.ExhibitsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.StatsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class OnlineActivity extends AppCompatActivity {
    private FirebaseDatabase mDatabase;
    private DatabaseReference mOnlineHQUsersRef, mOnlineUsersRef;
    private RecyclerView recyclerView;
    private UserRecycleAdapter userRecycleAdapter;
    private List<User> usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_online);
        mDatabase = FirebaseDatabase.getInstance();
        mOnlineHQUsersRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
        mOnlineUsersRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);

        usersList = new ArrayList<>();
        userRecycleAdapter = new UserRecycleAdapter(this, usersList);
        recyclerView = (RecyclerView) findViewById(R.id.usersListID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(userRecycleAdapter);
        mOnlineHQUsersRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                if(!usersList.contains(user)){

                    DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUserID());
                    userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                                @Override
                                public void onDataChange(DataSnapshot dataSnapshot) {
                                    User u1=dataSnapshot.getValue(User.class);
                                    if(!usersList.contains(u1)) {
                                        usersList.add(u1);
                                    }
                                    userRecycleAdapter.notifyDataSetChanged();
                                }

                                @Override
                                public void onCancelled(DatabaseError databaseError) {

                                }
                            });
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                User user2 = dataSnapshot.getValue(User.class);
                if(!user2.getUserID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                    Toast.makeText(OnlineActivity.this, "Admin Signed Out!", Toast.LENGTH_SHORT).show();
                    if(usersList.contains(user2)){
                        usersList.remove(user2);
                        userRecycleAdapter.notifyDataSetChanged();
                    }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mOnlineUsersRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                if(!usersList.contains(user)){

                    DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUserID());
                    userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User u1=dataSnapshot.getValue(User.class);
                            if(!usersList.contains(u1)) {
                                usersList.add(u1);
                            }
                            userRecycleAdapter.notifyDataSetChanged();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                User user2 = dataSnapshot.getValue(User.class);
                if(!user2.getUserID().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                    if(usersList.contains(user2)){
                        Toast.makeText(OnlineActivity.this, "User Signed Out!", Toast.LENGTH_SHORT).show();
                        usersList.remove(user2);
                        userRecycleAdapter.notifyDataSetChanged();
                    }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void goToLoginScreen(){
        Intent intent = new Intent(OnlineActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
