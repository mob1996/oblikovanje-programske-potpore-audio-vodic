package fer.ferplay.audiovodic.Activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.MutableData;
import com.google.firebase.database.Transaction;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import fer.ferplay.audiovodic.Data.ExhibitRecycleAdapter;
import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.Promo;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.Constants;
import fer.ferplay.audiovodic.Util.DatabaseNodes.ExhibitsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.StatsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.PromoNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class FrontPageActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private ExhibitRecycleAdapter exhibitRecycleAdapter;
    private List<Exhibit> exhibitList;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;
    private DatabaseReference mExhibitsRef;
    private DatabaseReference mPromosRef;
    private DatabaseReference generalStatsRef;
    private DatabaseReference frontPageStatsRef;
    private FirebaseUser mUser;

    private long beginTimeCount;
    private long endTimeCount;

    public static final int REQUEST_CODE = 100;
    public static final int PERMISSION_REQUEST =200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_front_page);
        Log.d("TAG", "ONCREATE");
        mAuth = FirebaseAuth.getInstance();
        mUser = mAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance();
        mExhibitsRef = mDatabase.getReference().child(ExhibitsNode.NODE_NAME);
        mPromosRef = mDatabase.getReference().child(PromoNode.NODE_NAME);
        generalStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_GENERAL);
        frontPageStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_FRONT_PAGE);

        beginTimeCount = System.currentTimeMillis();

        exhibitList = new ArrayList<>();
        recyclerView = (RecyclerView) findViewById(R.id.exhibitListID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("TAG", "ONSTART");
        Log.d("Ref", mExhibitsRef.toString());
        exhibitRecycleAdapter = new ExhibitRecycleAdapter(FrontPageActivity.this, exhibitList);
        recyclerView.setAdapter(exhibitRecycleAdapter);
        //Just Once It resets database values
        /*DatabaseReference generalRef = mDatabase.getReference().child("stats").child(StatsNode.CHILD_GENERAL);
        generalRef.child(StatsNode.CHILD_TOTAL_VISITS).setValue(0);
        generalRef.child(StatsNode.CHILD_TOTAL_REPRODUCTIONS).setValue(0);
        generalRef.child(StatsNode.CHILD_TOTAL_VISIT_TIME).setValue(0);

        DatabaseReference frontPageRef = mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_FRONT_PAGE);
        frontPageRef.child(StatsNode.CHILD_VISITS).setValue(0);
        frontPageRef.child(StatsNode.CHILD_VISIT_TIME).setValue(0);*/

        mExhibitsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Exhibit exhibit = dataSnapshot.getValue(Exhibit.class);
                if(!exhibitList.contains(exhibit)){
                    exhibitList.add(exhibit);
                    exhibitRecycleAdapter.notifyDataSetChanged();
                    /*DatabaseReference exhibitRef = mDatabase.getReference().child(StatsNode.NODE_NAME)
                            .child(StatsNode.CHILD_EXHIBITS_STATS).child(exhibit.getId());
                    exhibitRef.child(StatsNode.CHILD_VISITS).setValue(0);
                    exhibitRef.child(StatsNode.CHILD_VISIT_TIME).setValue(0);
                    exhibitRef.child(StatsNode.CHILD_REPRODUCTIONS).setValue(0);*/
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                Exhibit editedExhibit = dataSnapshot.getValue(Exhibit.class);
                int i=0;
                for(i=0; i<exhibitList.size();i++){
                    if(editedExhibit.getId().equals(exhibitList.get(i).getId())){
                        exhibitList.set(i,editedExhibit);
                        break;
                    }
                }
                exhibitRecycleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Exhibit removedExhibit = dataSnapshot.getValue(Exhibit.class);
                exhibitList.remove(removedExhibit);
                exhibitRecycleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //makeshift rjesenje
        mPromosRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Exhibit promo = dataSnapshot.getValue(Exhibit.class);
                if (!exhibitList.contains(promo) && !mUser.isAnonymous()) {
                    exhibitList.add(promo);
                    Collections.shuffle(exhibitList);

                   Exhibit last= exhibitList.get(exhibitList.size()-1);
                   Exhibit first= exhibitList.get(0);
                   while(last.getTitle().contains("promo") || first.getTitle().contains("promo")){
                       Collections.shuffle(exhibitList);
                       last= exhibitList.get(exhibitList.size()-1);
                   }
                    exhibitRecycleAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
        Log.d("OnResume", "Resume");
        exhibitRecycleAdapter.notifyDataSetChanged();
        generalStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long totalVisits = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long totalVisitsLong = (Long) data.get(StatsNode.CHILD_TOTAL_VISITS);
                    totalVisits = totalVisitsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                totalVisits++;;
                data.put(StatsNode.CHILD_TOTAL_VISITS, totalVisits);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

        frontPageStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long visits = 0;
                Map<String, Object> data = null;
                if(currentData.getValue()!=null){
                    data = (Map) currentData.getValue();
                    Long visitsLong = (Long) data.get(StatsNode.CHILD_VISITS);
                    visits = visitsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                visits++;
                data.put(StatsNode.CHILD_VISITS, visits);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
        if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
        endTimeCount = System.currentTimeMillis();
        generalStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long totalVisitTime = 0;
                Map<String, Object> data = null;
                if(currentData.getValue() != null){
                    data = (Map) currentData.getValue();
                    Long totalVisitTimeLong = (Long) data.get(StatsNode.CHILD_TOTAL_VISIT_TIME);
                    totalVisitTime = totalVisitTimeLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                totalVisitTime += (endTimeCount-beginTimeCount);
                data.put(StatsNode.CHILD_TOTAL_VISIT_TIME, totalVisitTime);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

        frontPageStatsRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData currentData) {
                long visitTime = 0;
                Map<String, Object> data = null;
                if(currentData.getValue()!=null){
                    data = (Map) currentData.getValue();
                    Long visitsLong = (Long) data.get(StatsNode.CHILD_VISIT_TIME);
                    visitTime = visitsLong.longValue();
                } else {
                    return Transaction.success(currentData);
                }
                visitTime += (endTimeCount-beginTimeCount);
                data.put(StatsNode.CHILD_VISIT_TIME, visitTime);
                currentData.setValue(data);
                return Transaction.success(currentData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b, DataSnapshot dataSnapshot) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.action_signout:
                actionSignOutFrontPageActitivty();
                break;

            case R.id.action_scan:
                scanForQR();
                break;

            case R.id.action_go_to_hq:
                startActivity(new Intent(this, HQActivity.class));
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        menu.findItem(R.id.action_go_to_front).setVisible(false);
        if(mUser.isAnonymous()){
            menu.findItem(R.id.action_go_to_hq).setVisible(false);
        } else {
            mDatabase.getReference().child("users").child(mAuth.getCurrentUser().getUid())
                    .addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            User user = dataSnapshot.getValue(User.class);
                            if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                                menu.findItem(R.id.action_go_to_hq).setVisible(false);
                            }
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void actionSignOutFrontPageActitivty(){
        if(mAuth.getCurrentUser()!=null){
            if(!mAuth.getCurrentUser().isAnonymous()){
                DatabaseReference userRef = mDatabase.getReference().child("users").child(mAuth.getCurrentUser().getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user = dataSnapshot.getValue(User.class);
                        DatabaseReference onlineRef;
                        if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                            onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                        } else {
                            onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                        }
                        onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                            @Override
                            public void onComplete(@NonNull Task<Void> task) {
                                mAuth.signOut();
                                Toast.makeText(FrontPageActivity.this, "Sign Out!", Toast.LENGTH_LONG).show();
                                goToLoginScreen();
                            }
                        });
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            } else {
                mAuth.signOut();
                goToLoginScreen();
            }
        }
    }

    private void scanForQR(){
        if(ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, PERMISSION_REQUEST);
        }

        Intent intent = new Intent(FrontPageActivity.this, CameraActivity.class);
        startActivityForResult(intent, REQUEST_CODE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == REQUEST_CODE && resultCode == RESULT_OK){
            if(data != null){
                Barcode barcode = data.getParcelableExtra(Constants.BARCODE_INTENT_KEY);
                Log.d("Barcode value", barcode.displayValue);
                DatabaseReference exhibitReference = mDatabase.getReference().child("exhibits").child(barcode.displayValue);
                exhibitReference.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if(!dataSnapshot.exists()){
                            Toast.makeText(FrontPageActivity.this, "Exhibit with that ID doesn't exists!", Toast.LENGTH_LONG).show();
                        } else {
                            Exhibit exhibit = dataSnapshot.getValue(Exhibit.class);
                            if(exhibit.getAudioLink().equals(Constants.NONE)){
                                Toast.makeText(FrontPageActivity.this, "Exhibit doesn't have audio track!", Toast.LENGTH_LONG).show();
                            } else {
                                Intent intent = new Intent(FrontPageActivity.this, PlayAudioActivity.class);
                                intent.putExtra(Constants.EXHIBIT_INTENT_KEY, exhibit);
                                startActivity(intent);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        }
    }

    public void goToLoginScreen(){
        Intent intent = new Intent(FrontPageActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }


}
