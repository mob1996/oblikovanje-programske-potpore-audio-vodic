package fer.ferplay.audiovodic.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import fer.ferplay.audiovodic.Data.ExhibitRecycleAdapter;
import fer.ferplay.audiovodic.Data.ExhibitStatsRecycleAdapter;
import fer.ferplay.audiovodic.Data.UserRecycleAdapter;
import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.ExhibitStats;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.ExhibitsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.StatsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class StatsActivity extends AppCompatActivity {

    private FirebaseDatabase mDatabase;
    private DatabaseReference mExhibitStatsRef;
    private RecyclerView recyclerView;
    private ExhibitStatsRecycleAdapter exhibitRecycleAdapter;
    private List<ExhibitStats> exhibitStatsList;
    private TextView totalReproductions, totalVisits, totalVisitTime;
    private TextView visitTimeFront, visitsFront;
    private boolean ascDesc=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stats);

        totalReproductions= findViewById(R.id.totalReproductions);
        totalVisits=findViewById(R.id.totalVisits);
        totalVisitTime = findViewById(R.id.totalVisitTime);
        visitsFront = findViewById(R.id.vistsFront);
        visitTimeFront = findViewById(R.id.visitTimeFront);

        mDatabase = FirebaseDatabase.getInstance();
        mExhibitStatsRef = mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_EXHIBITS_STATS);

        exhibitStatsList = new ArrayList<>();
        exhibitRecycleAdapter = new ExhibitStatsRecycleAdapter(this, exhibitStatsList);
        recyclerView = (RecyclerView) findViewById(R.id.exhibitListStats);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(exhibitRecycleAdapter);
        mExhibitStatsRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                ExhibitStats exhibitStats= dataSnapshot.getValue(ExhibitStats.class);
                mDatabase.getReference().child(ExhibitsNode.NODE_NAME).child(dataSnapshot.getKey())
                        .addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                Exhibit exhibit = dataSnapshot.getValue(Exhibit.class);
                                exhibitStats.setImageLink(exhibit.getImageLink());
                                exhibitStats.setTitle(exhibit.getTitle());
                                exhibitStatsList.add(exhibitStats);
                                exhibitRecycleAdapter.notifyDataSetChanged();
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {

                            }
                        });
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_GENERAL).child(StatsNode.CHILD_TOTAL_VISITS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long totalVisitss = dataSnapshot.getValue(Long.class);
                        totalVisits.setText("Total visits: " + totalVisitss);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_GENERAL).child(StatsNode.CHILD_TOTAL_REPRODUCTIONS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long totalReproductionss = dataSnapshot.getValue(Long.class);
                        totalReproductions.setText("Total reproductions: " + totalReproductionss);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_GENERAL).child(StatsNode.CHILD_TOTAL_VISIT_TIME)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long totalVisitTimes = dataSnapshot.getValue(Long.class);
                        totalVisitTime.setText(String.format("Total visit time: %.2f min", ((float)totalVisitTimes/(1000*60))));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_FRONT_PAGE).child(StatsNode.CHILD_VISIT_TIME)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long visitTime = dataSnapshot.getValue(Long.class);
                        visitTimeFront.setText(String.format("Total visit time: %.2f min", ((float)visitTime/(1000*60))));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
        mDatabase.getReference().child(StatsNode.NODE_NAME).child(StatsNode.CHILD_FRONT_PAGE).child(StatsNode.CHILD_VISITS)
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Long visits = dataSnapshot.getValue(Long.class);
                        visitsFront.setText("Total visits: " + visits);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }

    public void goToLoginScreen(){
        Intent intent = new Intent(StatsActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.visit_time_sort:
                if(ascDesc) {
                    ascDesc=!ascDesc;
                    Collections.sort(exhibitStatsList, ExhibitStats.BY_VISIT_TIME_ASC);
                    exhibitRecycleAdapter.notifyDataSetChanged();
                }else{
                    ascDesc=!ascDesc;
                    Collections.sort(exhibitStatsList, ExhibitStats.BY_VISIT_TIME_DESC);
                    exhibitRecycleAdapter.notifyDataSetChanged();
                }
                break;

            case R.id.visits_sort:
                if(ascDesc) {
                    ascDesc = !ascDesc;
                    Collections.sort(exhibitStatsList, ExhibitStats.BY_VISITS_ASC);
                    exhibitRecycleAdapter.notifyDataSetChanged();
                }else{
                    ascDesc=!ascDesc;
                    Collections.sort(exhibitStatsList, ExhibitStats.BY_VISITS_DESC);
                    exhibitRecycleAdapter.notifyDataSetChanged();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.stats_sort_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
