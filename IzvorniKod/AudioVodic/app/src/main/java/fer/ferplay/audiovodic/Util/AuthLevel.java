package fer.ferplay.audiovodic.Util;

/**
 * Created by Mob on 29.10.2017..
 */

public class AuthLevel {
    public static final String AUTH_OWNER = "owner";
    public static final String AUTH_ADMIN = "admin";
    public static final String AUTH_USER = "user";
}
