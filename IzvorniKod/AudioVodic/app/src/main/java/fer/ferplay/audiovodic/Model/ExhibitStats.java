package fer.ferplay.audiovodic.Model;

import java.util.Comparator;

/**
 * Created by matej on 23/12/2017.
 */

public class ExhibitStats {
    private Long reproductions;
    private Long visitTime;
    private Long visits;
    private String title;
    private String imageLink;

    public ExhibitStats(Long reproductions, Long visitTime, Long visits, String title, String imageLink) {
        this.reproductions = reproductions;
        this.visitTime = visitTime;
        this.visits = visits;
        this.title = title;
        this.imageLink = imageLink;
    }

    public ExhibitStats(Long reproductions, Long visitTime, Long visits) {
        this.reproductions = reproductions;
        this.visitTime = visitTime;
        this.visits = visits;
    }

    public ExhibitStats() {
    }

    public static final Comparator<ExhibitStats> BY_VISITS_DESC = new Comparator<ExhibitStats>() {
        @Override
        public int compare(ExhibitStats o1, ExhibitStats o2) {
            return o2.getVisits().compareTo(o1.getVisits());
        }
    };

    public static final Comparator<ExhibitStats> BY_VISIT_TIME_DESC = new Comparator<ExhibitStats>() {
        @Override
        public int compare(ExhibitStats o1, ExhibitStats o2) {
            return o2.getVisitTime().compareTo(o1.getVisitTime());
        }
    };

    public static final Comparator<ExhibitStats> BY_VISITS_ASC = new Comparator<ExhibitStats>() {
        @Override
        public int compare(ExhibitStats o1, ExhibitStats o2) {
            return o1.getVisits().compareTo(o2.getVisits());
        }
    };

    public static final Comparator<ExhibitStats> BY_VISIT_TIME_ASC = new Comparator<ExhibitStats>() {
        @Override
        public int compare(ExhibitStats o1, ExhibitStats o2) {
            return o1.getVisitTime().compareTo(o2.getVisitTime());
        }
    };

    public Long getReproductions() {
        return reproductions;
    }

    public void setReproductions(Long reproductions) {
        this.reproductions = reproductions;
    }

    public Long getVisitTime() {
        return visitTime;
    }

    public void setVisitTime(Long visitTime) {
        this.visitTime = visitTime;
    }

    public Long getVisits() {
        return visits;
    }

    public void setVisits(Long visits) {
        this.visits = visits;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }
}
