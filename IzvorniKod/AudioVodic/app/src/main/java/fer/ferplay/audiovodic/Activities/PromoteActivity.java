package fer.ferplay.audiovodic.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import fer.ferplay.audiovodic.Data.DemoteRecycleAdapter;
import fer.ferplay.audiovodic.Data.PromoteRecycleAdapter;
import fer.ferplay.audiovodic.Model.Admin;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.AdminsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

/**
 * Created by Karlo on 13.1.2018..
 */

public class PromoteActivity   extends AppCompatActivity {

    private FirebaseDatabase mDatabase;
    private DatabaseReference mAdminRef;
    private RecyclerView recyclerView;
    private PromoteRecycleAdapter userRecycleAdapter;
    private List<User> userList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promote);

        mDatabase = FirebaseDatabase.getInstance();
        mAdminRef = mDatabase.getReference().child(UsersNode.NODE_NAME);

        userList = new ArrayList<>();
        userRecycleAdapter = new PromoteRecycleAdapter(this, userList);
        recyclerView = (RecyclerView) findViewById(R.id.promoteListID);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(userRecycleAdapter);

        Toast.makeText(PromoteActivity.this,
                "Click on user to promote him to admin",
                Toast.LENGTH_SHORT).show();
        mAdminRef.addChildEventListener(new ChildEventListener() {

            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);

                if(!userList.contains(user) && user.getAuthLevel().equals(AuthLevel.AUTH_USER)) {
                    userList.add(user);
                    userRecycleAdapter.notifyItemInserted(userList.size() - 1);
                }

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                User user = dataSnapshot.getValue(User.class);
                if(user!=null) {
                    int i = -1;
                    for (User u : userList)
                        if (u.getUserID().equals(user.getUserID()))
                            i=userList.indexOf(u);
                    if(i!=-1) {
                        Toast.makeText(PromoteActivity.this, "user removed from list", Toast.LENGTH_SHORT).show();
                        userList.remove(i);
                        userRecycleAdapter.notifyDataSetChanged();
                    }
                }
            }



            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(userList.contains(user)){
                    int pos = userList.indexOf(user);
                    userList.remove(pos);
                }
                userRecycleAdapter.notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            if(!user.isAnonymous()) {
                DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(user.getUid());
                userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        User user1 = dataSnapshot.getValue(User.class);
                        if (user1.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user1.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)) {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        } else {
                            DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                                    .child(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_ID).setValue(user1.getUserID());
                            onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user1.getEmail());
                            onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user1.getAuthLevel());
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                    }
                });
            }
        } else {
            goToLoginScreen();
        }
        userRecycleAdapter.brojAdmina=-1;
        FirebaseDatabase.getInstance().getReference().child(AdminsNode.NODE_NAME)
                .addChildEventListener(new ChildEventListener() {
                    @Override
                    public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                        userRecycleAdapter.brojAdmina++;
                    }

                    @Override
                    public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onChildRemoved(DataSnapshot dataSnapshot) {
                        userRecycleAdapter.brojAdmina--;
                    }

                    @Override
                    public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }

    @Override
    protected void onPause() {
        super.onPause();
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if(user!=null)
            if(!user.isAnonymous()){
            DatabaseReference userRef = mDatabase.getReference().child("users").child(user.getUid());
            userRef.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    User user = dataSnapshot.getValue(User.class);
                    DatabaseReference onlineRef;
                    if(user.getAuthLevel().equals(AuthLevel.AUTH_USER)){
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS);
                    } else {
                        onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ);
                    }
                    onlineRef.child(user.getUserID()).removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
                        @Override
                        public void onComplete(@NonNull Task<Void> task) {
                        }
                    });
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });
        }
    }
    public void goToLoginScreen(){
        Intent intent = new Intent(PromoteActivity.this, LoginActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

}
