package fer.ferplay.audiovodic.Data;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import fer.ferplay.audiovodic.Activities.ExhibitActivity;
import fer.ferplay.audiovodic.Model.Exhibit;
import fer.ferplay.audiovodic.Model.ExhibitStats;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.Constants;

/**
 * Created by matej on 23/12/2017.
 */

public class ExhibitStatsRecycleAdapter extends RecyclerView.Adapter<ExhibitStatsRecycleAdapter.ViewHolder>{

    private Context context;
    private List<ExhibitStats> exhibitListStats;

    public ExhibitStatsRecycleAdapter(Context context, List<ExhibitStats> exhibitList) {
        this.context = context;
        this.exhibitListStats = exhibitList;
    }

    @Override
    public ExhibitStatsRecycleAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.exhibit_stats, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ExhibitStatsRecycleAdapter.ViewHolder holder, int position) {
        ExhibitStats exhibitStats = exhibitListStats.get(position);
        String imageUrl = null;

        holder.exhibitVisits.setText("Visits: " + exhibitStats.getVisits());
        holder.exhibitReproductions.setText("Reproductions: " + exhibitStats.getReproductions());
        holder.exhibitTitle.setText(exhibitStats.getTitle());
        holder.exhibitVisitTime.setText(String.format("Visit time: %.2f min", ((float)exhibitStats.getVisitTime()/(1000*60))));

        imageUrl = exhibitStats.getImageLink();
        if(!imageUrl.equals(Constants.NONE)){
            Picasso.with(context).load(imageUrl).into(holder.exhibitImage);
        }
    }

    @Override
    public int getItemCount() {
        return exhibitListStats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{
        public ImageView exhibitImage;
        public TextView exhibitTitle;
        public TextView exhibitReproductions;
        public TextView exhibitVisitTime;
        public TextView exhibitVisits;

        public ViewHolder(View view, Context ctx) {
            super(view);
            exhibitImage = view.findViewById(R.id.rowExhibitImageIDStats);
            exhibitTitle = view.findViewById(R.id.exhibitTitleIDStats);
            exhibitReproductions = view.findViewById(R.id.totalReproductionsExhibitStats);
            exhibitVisitTime = view.findViewById(R.id.visitTimeExhibitStats);
            exhibitVisits = view.findViewById(R.id.visitsExhibitStats);
        }

    }
}
