package fer.ferplay.audiovodic.Util.DatabaseNodes;

/**
 * Created by Mob on 25.10.2017..
 */

public class UsersNode {
    public static final String NODE_NAME = "users";
    public static final String CHILD_AUTH = "authLevel";
    public static final String CHILD_EMAIL = "email";
    public static final String CHILD_FNAME = "first_name";
    public static final String CHILD_LNAME = "last_name";
    public static final String CHILD_PROMO = "promo";
    public static final String CHILD_ID = "userID";
}
