package fer.ferplay.audiovodic.Model;

import java.io.Serializable;

import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.Constants;

/**
 * Created by Mob on 23.10.2017..
 */

public class User implements Serializable {
    private String authLevel;
    private String email;
    private String first_name;
    private String last_name;
    private String userID;

    public User() {
    }

    //za demote/promote
    public User(Admin admin){
        this.authLevel= AuthLevel.AUTH_ADMIN;
        this.email=admin.getEmail();
        this.userID=admin.getID();
        this.first_name=Constants.NONE;
        this.last_name= Constants.NONE;
    }

    public User(String authLevel, String email, String first_name, String last_name, String userID) {
        this.authLevel = authLevel;
        this.email = email;
        this.first_name = first_name;
        this.last_name = last_name;
        this.userID = userID;
    }

    public String getAuthLevel() {
        return authLevel;
    }

    public void setAuthLevel(String authLevel) {
        this.authLevel = authLevel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;
        if(userID.equals(((User) o).getUserID())) return true;
        if (authLevel != null ? !authLevel.equals(user.authLevel) : user.authLevel != null)
            return false;
        if (email != null ? !email.equals(user.email) : user.email != null) return false;
        if (first_name != null ? !first_name.equals(user.first_name) : user.first_name != null)
            return false;
        if (last_name != null ? !last_name.equals(user.last_name) : user.last_name != null)
            return false;
        return userID != null ? userID.equals(user.userID) : user.userID == null;

    }

    @Override
    public int hashCode() {
        int result = authLevel != null ? authLevel.hashCode() : 0;
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (first_name != null ? first_name.hashCode() : 0);
        result = 31 * result + (last_name != null ? last_name.hashCode() : 0);
        result = 31 * result + (userID != null ? userID.hashCode() : 0);
        return result;
    }
}
