package fer.ferplay.audiovodic.Model;

import java.io.Serializable;

/**
 * Created by Mob on 24.10.2017..
 */

public class Exhibit implements Serializable {

    private String id;
    private String title;
    private String description;
    private String imageLink;
    private String audioLink;

    public Exhibit(){

    }

    public Exhibit(String id, String title, String description, String imageLink, String audioLink) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.imageLink = imageLink;
        this.audioLink = audioLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageLink() {
        return imageLink;
    }

    public void setImageLink(String imageLink) {
        this.imageLink = imageLink;
    }

    public String getAudioLink() {
        return audioLink;
    }

    public void setAudioLink(String audioLink) {
        this.audioLink = audioLink;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Exhibit exhibit = (Exhibit) o;

        return id.equals(exhibit.id);

    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }
}
