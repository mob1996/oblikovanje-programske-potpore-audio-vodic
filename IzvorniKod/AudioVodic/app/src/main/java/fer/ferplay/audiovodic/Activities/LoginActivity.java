package fer.ferplay.audiovodic.Activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.OnlineNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class LoginActivity extends AppCompatActivity {

    private EditText emailField;
    private EditText passwordField;
    private Button loginBtn;
    private Button guestBtn;
    private Button createAccountBtn;

    private FirebaseAuth.AuthStateListener mAuthListener;
    private FirebaseAuth mAuth;
    private FirebaseUser mUser;
    private FirebaseDatabase mDatabase;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
                super.onCreate(savedInstanceState);
                setContentView(R.layout.activity_login);

                emailField = (EditText) findViewById(R.id.loginEmailId);
                passwordField = (EditText) findViewById(R.id.loginPasswordId);
                loginBtn = (Button) findViewById(R.id.loginBtn);
                guestBtn = (Button) findViewById(R.id.loginGuestBtn);
                createAccountBtn = (Button) findViewById(R.id.loginCreateAccountBtn);

                mAuth = FirebaseAuth.getInstance();
                mDatabase = FirebaseDatabase.getInstance();
                /* Kod za log out pri ulasku u program
                if(mAuth.getCurrentUser()!=null){
                    mAuth.signOut();
                }
                */
                mAuthListener = new FirebaseAuth.AuthStateListener() {
                    @Override
                    public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
                        mUser = firebaseAuth.getCurrentUser();

                        if(mUser != null){
                            if(mUser.isAnonymous()){
                                Toast.makeText(LoginActivity.this, "Continue as a guest!", Toast.LENGTH_LONG).show();
                                goToFrontPage();
                            } else {
                                if(mUser.isEmailVerified()){
                                    //Adding User to online branch in database and going to next acitivity
                                    pushUserOnlineAndGoToNextAcitvity();
                                } else {
                                    //If user email is not verified, create a window that asks them if they want new verification email.
                                    askForAnotherVerificationEmail();
                                }
                            }

                        }
                    }
                };

                loginBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String email = emailField.getText().toString();
                        String password = passwordField.getText().toString();

                        if(!email.isEmpty() && !password.isEmpty()){
                            // User Login
                            mAuth.signInWithEmailAndPassword(email, password).addOnFailureListener(new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Toast.makeText(LoginActivity.this, "Error while trying to sign you in!", Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            Toast.makeText(LoginActivity.this, "Email and password field must not be empty!", Toast.LENGTH_LONG).show();
                        }
                    }
                });


                createAccountBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        startActivity(new Intent(LoginActivity.this, CreateAccountActivity.class));
                    }
                });

                guestBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mAuth.signInAnonymously();
                    }
                });

    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if(mAuth!=null){
            mAuth.removeAuthStateListener(mAuthListener);
        }
    }


    private void pushUserOnlineAndGoToNextAcitvity(){
        DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(mUser.getUid());
        builder = new AlertDialog.Builder(LoginActivity.this);
        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.progress_dialog, null);
        TextView progressMessage = (TextView) view.findViewById(R.id.progressTextViewID);
        progressMessage.setText("Signing in!");
        builder.setView(view);
        builder.setCancelable(false);
        alertDialog = builder.create();
        alertDialog.show();
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                if(user.getAuthLevel().equals(AuthLevel.AUTH_OWNER) || user.getAuthLevel().equals(AuthLevel.AUTH_ADMIN)){
                    DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_HQ)
                            .child(user.getUserID());
                    onlineRef.child(OnlineNode.CHILD_ID).setValue(user.getUserID());
                    onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user.getEmail());
                    onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user.getAuthLevel());
                    alertDialog.dismiss();
                    //TODO: go to HQ, remove goToFrontPage() and dont forget to finish() after startActivity
                    goToFrontPage();
                } else {
                    DatabaseReference onlineRef = mDatabase.getReference().child(OnlineNode.NODE_NAME).child(OnlineNode.CHILD_USERS)
                            .child(user.getUserID());
                    onlineRef.child(OnlineNode.CHILD_ID).setValue(user.getUserID());
                    onlineRef.child(OnlineNode.CHILD_EMAIL).setValue(user.getEmail());
                    onlineRef.child(OnlineNode.CHILD_AUTH).setValue(user.getAuthLevel());
                    alertDialog.dismiss();
                    goToFrontPage();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }

    private void askForAnotherVerificationEmail(){
        AlertDialog.Builder builder = new AlertDialog.Builder(LoginActivity.this);
        builder.setMessage("Do you want another verification email?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mUser.sendEmailVerification();
                mAuth.signOut();
                dialogInterface.cancel();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                mAuth.signOut();
                dialogInterface.cancel();
            }
        });
        Toast.makeText(LoginActivity.this, "Please verify your account!", Toast.LENGTH_LONG).show();
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    private void goToFrontPage(){
        Intent frontPageIntent = new Intent(LoginActivity.this, FrontPageActivity.class);
        startActivity(frontPageIntent);
        finish();
    }

}
