package fer.ferplay.audiovodic.Data;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.List;

import fer.ferplay.audiovodic.Activities.DemoteActivity;
import fer.ferplay.audiovodic.Activities.PromoteActivity;
import fer.ferplay.audiovodic.Model.Admin;
import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.AdminsNode;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

import static android.support.v4.content.ContextCompat.startActivity;

/**
 * Created by Karlo on 13.1.2018..
 */

public class PromoteRecycleAdapter extends RecyclerView.Adapter<PromoteRecycleAdapter.ViewHolder> {
    private FirebaseDatabase mDatabase;
    private Context context;
    private List<User> userList;
    private Admin admin;
    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;
    public int brojAdmina=-1;

    public PromoteRecycleAdapter(Context context, List<User> userList) {
        this.context = context;
        this.userList = userList;
        mDatabase = FirebaseDatabase.getInstance();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.online_user, parent, false);
        return new ViewHolder(view, context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        User user = userList.get(position);

        holder.userEmail.setText(user.getEmail());
        holder.userAuthLevel.setText("Auth level: " + user.getAuthLevel());
        holder.userFirstName.setText("First Name: " + user.getFirst_name());
        holder.userLastName.setText("Last Name: " + user.getLast_name());
    }

    @Override
    public int getItemCount() {
        return userList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView userEmail;
        public TextView userAuthLevel;
        public TextView userFirstName;
        public TextView userLastName;
        private Context context;

        public ViewHolder(View view, Context ctx) {
            super(view);
            context=ctx;
            userEmail = (TextView) view.findViewById(R.id.userEmail);
            userAuthLevel = (TextView) view.findViewById(R.id.userAuthLevel);
            userFirstName=view.findViewById(R.id.userFirstName);
            userLastName=view.findViewById(R.id.userLastName);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (brojAdmina < 5) {
                User user = userList.get(getAdapterPosition());
                Admin admin = new Admin(user.getEmail(), user.getUserID());
                DatabaseReference databaseRef = mDatabase.getReference()
                        .child(UsersNode.NODE_NAME).child(user.getUserID()).child(UsersNode.CHILD_AUTH);

                DatabaseReference adminRef = mDatabase.getReference().child(AdminsNode.NODE_NAME).child(admin.getID());

                builder = new AlertDialog.Builder(context);

                builder.setTitle("Warning")
                        .setMessage("Are you sure you want to promote this user to admin?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {

                                databaseRef.setValue(AuthLevel.AUTH_ADMIN).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                    }
                                });
                                adminRef.setValue(admin).addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {

                                    }
                                });
                            }
                        })
                        .setNegativeButton(android.R.string.no, null).show();
            } else {
                Toast.makeText(context, "Maximum number of admins reached", Toast.LENGTH_SHORT).show();
            }
        }

    }
}
