package fer.ferplay.audiovodic.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import fer.ferplay.audiovodic.Model.User;
import fer.ferplay.audiovodic.R;
import fer.ferplay.audiovodic.Util.AuthLevel;
import fer.ferplay.audiovodic.Util.DatabaseNodes.UsersNode;

public class CreateAccountActivity extends AppCompatActivity {

    private EditText firstNameField;
    private EditText lastNameField;
    private EditText emailField;
    private EditText passwordField;
    private EditText repeatPasswordField;
    private Button createAccBtn;

    private FirebaseAuth mAuth;
    private FirebaseDatabase mDatabase;

    private AlertDialog.Builder builder;
    private AlertDialog alertDialog;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_account);

        firstNameField = (EditText) findViewById(R.id.createFirstNameId);
        lastNameField = (EditText) findViewById(R.id.createLastNameId);
        emailField = (EditText) findViewById(R.id.createEmailId);
        passwordField = (EditText) findViewById(R.id.createPasswordId);
        repeatPasswordField = (EditText) findViewById(R.id.createRepeatPasswordId);
        createAccBtn = (Button) findViewById(R.id.createCreateAccountBtn);

        mAuth = FirebaseAuth.getInstance();
        mDatabase = FirebaseDatabase.getInstance();


        createAccBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final String firstName = firstNameField.getText().toString();
                final String lastName = lastNameField.getText().toString();
                String email = emailField.getText().toString();
                String pwd = passwordField.getText().toString();
                String repwd = repeatPasswordField.getText().toString();

                if(!firstName.isEmpty() && !lastName.isEmpty() && !email.isEmpty()
                        && !pwd.isEmpty() && !repwd.isEmpty()){

                    if(pwd.equals(repwd)){
                        builder = new AlertDialog.Builder(CreateAccountActivity.this);
                        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        builder.setView(inflater.inflate(R.layout.progress_dialog, null));
                        builder.setCancelable(false);
                        alertDialog = builder.create();
                        mAuth.createUserWithEmailAndPassword(email, pwd).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                if(task.isSuccessful()){
                                    alertDialog.show();
                                    final FirebaseUser currentUser = mAuth.getCurrentUser();
                                    DatabaseReference userRef = mDatabase.getReference().child(UsersNode.NODE_NAME).child(currentUser.getUid());
                                    user = new User(AuthLevel.AUTH_USER, currentUser.getEmail(), firstName, lastName, currentUser.getUid());


                                    userRef.setValue(user).addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            currentUser.sendEmailVerification().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    mAuth.signOut();
                                                    startActivity(new Intent(CreateAccountActivity.this, LoginActivity.class));
                                                    alertDialog.dismiss();
                                                    finish();
                                                }
                                            });
                                        }
                                    });

                                } else {
                                    Toast.makeText(CreateAccountActivity.this, "There was a problem creating this account.", Toast.LENGTH_LONG).show();
                                }
                            }
                        });

                    } else {
                        Toast.makeText(CreateAccountActivity.this, "Passwords don't match!", Toast.LENGTH_LONG).show();
                    }

                } else {
                    Toast.makeText(CreateAccountActivity.this, "Please fill out all of the information", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

}
