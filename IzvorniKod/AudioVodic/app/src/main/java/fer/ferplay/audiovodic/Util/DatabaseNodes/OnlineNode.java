package fer.ferplay.audiovodic.Util.DatabaseNodes;

/**
 * Created by Mob on 25.10.2017..
 */

public class OnlineNode {
    public static final String NODE_NAME = "online";
    public static final String CHILD_HQ = "hq";
    public static final String CHILD_USERS = "users";
    public static final String CHILD_ID = "userID";
    public static final String CHILD_EMAIL = "email";
    public static final String CHILD_AUTH = "authLevel";
}
